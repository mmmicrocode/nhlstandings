//
//  UIKitExtensions.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/10/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import Foundation

extension Date {
    var nhlScheduleShortDisplayFormat : String {
           let formatter = DateFormatter()
           formatter.dateFormat = "MMM dd, yyyy"
           return formatter.string(from: self)
       }
}

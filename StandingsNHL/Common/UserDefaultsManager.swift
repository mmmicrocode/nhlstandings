//
//  UserDefaultsManager.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 1/28/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import Foundation

class UserDefaultsManager: ObservableObject {
    @Published var favoriteDivision: Int = UserDefaults.standard.integer(forKey: "favoriteDivision") {
        didSet { UserDefaults.standard.set(self.favoriteDivision, forKey: "favoriteDivision") }
    }

    @Published var favoriteTeam: Int = UserDefaults.standard.integer(forKey: "favoriteTeam") {
        didSet { UserDefaults.standard.set(self.favoriteTeam, forKey: "favoriteTeam") }
    }
}

//
//  ScheduleGameContentView.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/7/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI
import NHLAPI
//import VideoPlayer

#if os(watchOS)
typealias ScheduleGameContentView = WatchScheduleGameContentView
#else
typealias ScheduleGameContentView = GenericScheduleGameContentView
#endif

#if !os(watchOS)
struct GenericScheduleGameContentView: View {
    @ObservedObject var gameContentVM = GameContentViewModel()
    var game : Game
    @State var play : Bool = true
    
    var body: some View {
        HStack(alignment: .top) {
            VStack {
                ScheduleGameRowView(game: game)
                    .padding(8.0)
                
                if self.game.status.abstractGameState == .final {
                    List {
                        Section(header: Text("Recap"), content: {
                            if self.gameContentVM.recapVM != nil && self.gameContentVM.recapVM!.recapVideoUrlString != nil {
                                VideoPlayerContainerView(urlString: self.gameContentVM.recapVM!.recapVideoUrlString!, startPlay: false)
                                    .frame(height: CGFloat(180.0))
                                //VideoPlayer(url: URL(string: self.gameContentVM.recapVideoVM!.recapVideoUrlString!)!, play: self.$play)
                            }
                        })
                        if self.gameContentVM.highlightVMs != nil {
                            Section(header: Text("Highlights"), content: {
                                ForEach(self.gameContentVM.highlightVMs!) { highlight in
                                    VStack(spacing: 8.0) {
                                        Text(highlight.highlightTitle)
                                        VideoPlayerContainerView(urlString: highlight.highlightVideoUrlString, startPlay : false)
                                        .frame(height: CGFloat(180.0))
                                    }
                                .padding()
                                }
                            })
                        }
                    }
                .listStyle(GroupedListStyle())
                }
                else {
                    if self.gameContentVM.previewVM != nil {
                        Text("\(self.gameContentVM.previewVM!.previewHeadline ?? "(no headline available)")")
                            .font(.headline)
                        Text("\(self.gameContentVM.previewVM!.previewSubhead ?? "")")
                            .font(.subheadline)
                    }
                }
                
                Spacer()
            }
        }
        .padding(8.0)
        //.navigationBarTitle("\(self.game.status.abstractGameState == .final ? "Game Recap" : "Game Preview")")
        .onAppear {
            self.gameContentVM.reloadGameContent(gameId: self.game.gameId)
        }
        .onDisappear {
            
        }
    }
}

//if self.game.status.abstractGameState == .final {
//    if self.gameContentVM.recapVideoVM != nil && self.gameContentVM.recapVideoVM!.recapImageUrlString != nil  {
//        ImageView(withURL: self.gameContentVM.recapVideoVM!.recapImageUrlString!)
//    }
//
//    if self.gameContentVM.recapVideoVM != nil && self.gameContentVM.recapVideoVM!.recapVideoUrlString != nil {
//        VideoPlayerContainerView(urlString: self.gameContentVM.recapVideoVM!.recapVideoUrlString!)
//        //VideoPlayer(url: URL(string: self.gameContentVM.recapVideoVM!.recapVideoUrlString!)!, play: self.$play)
//    }
//}
#endif

struct WatchScheduleGameContentView: View {
    @ObservedObject var gameContentVM = GameContentViewModel()
    var game : Game
    @State var play : Bool = true

    var body: some View {
        HStack(alignment: .top) {
            VStack {
                ScheduleGameRowView(game: game)
                    .padding(8.0)
            }
        }
    }
}

//struct ScheduleGameContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ScheduleGameContentView()
//    }
//}

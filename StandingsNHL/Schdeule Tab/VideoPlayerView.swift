//
//  VideoPlayerView.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/11/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI
import AVFoundation
import UIKit

struct VideoPlayerView: UIViewRepresentable {
    let player: AVPlayer
    var startPlay : Bool = false

    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<VideoPlayerView>) {
    }
  
    func makeUIView(context: Context) -> UIView {
        return VideoPlayerUIView(player: player, startPlay: startPlay)
    }
}

struct VideoPlayerContainerView : View {
    private let player: AVPlayer
    var urlString : String
    var startPlay : Bool = false

    init(urlString: String, startPlay: Bool = false) {
        self.urlString = urlString
        self.startPlay = startPlay
        self.player = AVPlayer(url: URL(string: urlString)!)
    }
  
    var body: some View {
        VStack {
            VideoPlayerView(player: player, startPlay: startPlay)
            VideoControlsView(player: player)
        }
    }
}

struct VideoControlsView : View {
    @State var playerPaused = true
    @State var seekPos = 0.0
    let player: AVPlayer
    
    var body: some View {
        Button(action: {
          self.playerPaused.toggle()
          if self.playerPaused {
            self.player.pause()
          }
          else {
            self.player.play()
          }
        }) {
          Image(systemName: playerPaused ? "play" : "pause")
        }
        
//        HStack {
//            Button(action: {
//                self.playerPaused.toggle()
//                if self.playerPaused {
//                    self.player.pause()
//                }
//                else {
//                    self.player.play()
//                }
//            }) {
//            Image(systemName: playerPaused ? "play" : "pause")
//                .padding(.leading, CGFloat(20))
//                .padding(.trailing, CGFloat(20))
//            }
//
//            Slider(value: $seekPos, from: 0, through: 1, onEditingChanged: { _ in
//                guard let item = self.player.currentItem else {
//                    return
//                }
//
//                let targetTime = self.seekPos * item.duration.seconds
//                self.player.seek(to: CMTime(seconds: targetTime, preferredTimescale: 600))
//            })
//           .padding(.trailing, CGFloat(20))
//        }
    }
}
    
class VideoPlayerUIView: UIView {
    private let playerLayer = AVPlayerLayer()

    init(player: AVPlayer, startPlay: Bool = false) {
        super.init(frame: .zero)
        playerLayer.player = player
        if startPlay {
            player.play()
        }
        layer.addSublayer(playerLayer)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
  
    override func layoutSubviews() {
        super.layoutSubviews()
        playerLayer.frame = bounds
    }
}

//
//  ScheduleGameRowView.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/7/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI
import UIKit
import NHLAPI

#if os(watchOS)
typealias ScheduleGameRowView = WatchScheduleGameRowView
#else
typealias ScheduleGameRowView = GenericScheduleGameRowView
#endif

#if !os(watchOS)
struct GenericScheduleGameRowView: View {
    var game : Game

    var body: some View {
        let away = game.teams.away
        let home = game.teams.home
        let gameState = game.status.abstractGameState
        
        let logoHeight = gameState == .preview ? CGFloat(100.0) : CGFloat(80.0)
        let logoWidth = gameState == .preview ? CGFloat(120.0) : CGFloat(100.0)
        
        let vsImage = gameState == .preview ? "at" : "minus"
        let vsHeight = gameState == .preview ? CGFloat(40.0) : CGFloat(10.0)
        let vsWidth = vsHeight
        
        let scoreHeight = CGFloat(45.0)
        let scoreWidth = scoreHeight
        let scoreRowHeight = CGFloat(130.0)
        let scoreCornerRadius = CGFloat(8.0)
        
        let gameStateFontSize = CGFloat(12.0)
        let gameStateRowHeight = CGFloat(24.0)
        let gameStateCornerRadius = CGFloat(4.0)
        
        var gameStateForeground : Color = Color.white
        var gameStateBackground : Color {
            switch(self.game.status.abstractGameState) {
            case .preview:
                return Color(UIColor.systemBlue)
            case .live:
                return Color(UIColor.systemGreen)
            case .final:
                return Color(UIColor.systemGray)
                //Color.black
            }
        }
        var gameScoreFontSize = CGFloat(32.0)
        var gameScoreForeground : Color = Color.white
        var gameScoreBackground : Color {
            switch(self.game.status.abstractGameState) {
            case .preview:
                return Color(UIColor.systemGray)
            case .live:
                return Color(UIColor.systemGray)
            case .final:
                return Color(UIColor.systemGray)
                //Color.black
            }
        }

        return VStack(alignment: .center, spacing: 0.0, content: {
            HStack(alignment: .center, spacing: 20.0, content: {
                ZStack(alignment: .bottomTrailing) {
                    Image(away.team.name)
                        .resizable()
                        .scaledToFit()
                        .padding([.top, .trailing], -10.0)
                        .padding(.leading, -6.0)
                        .frame(width: logoWidth, height: logoHeight, alignment: .center)

//                    if gameState != .preview {
//                        Text(" \(away.score) ")
//                            .padding(2.0)
//                            .foregroundColor(gameScoreForeground)
//                            .background(gameScoreBackground)
//                            .font(Font.custom("HelveticaNeue-Bold", size: gameScoreFontSize))
//                            .cornerRadius(scoreCornerRadius)
//                    }
                }

                if gameState != .preview {
                    Text(" \(away.score) ")
                        .padding(0.0)
                        .foregroundColor(gameScoreForeground)
                        .background(gameScoreBackground)
                        .font(Font.custom("HelveticaNeue-Bold", size: gameScoreFontSize))
                        .cornerRadius(scoreCornerRadius)
                        .frame(width: scoreWidth, height: scoreHeight, alignment: .center)
                }
                
                Image(systemName: vsImage)
                    .resizable()
                    .padding(0.0)
                    .aspectRatio(contentMode:.fit)
                    .frame(width: vsWidth, height: vsHeight, alignment: .center)

                if gameState != .preview {
                    Text(" \(home.score) ")
                        .padding(0.0)
                        .foregroundColor(gameScoreForeground)
                        .background(gameScoreBackground)
                        .font(Font.custom("HelveticaNeue-Bold", size: gameScoreFontSize))
                        .cornerRadius(scoreCornerRadius)
                        .frame(width: scoreWidth, height: scoreHeight, alignment: .center)
                }
                
                ZStack(alignment: .bottomLeading) {
                    Image(home.team.name)
                        .resizable()
                        .scaledToFit()
                        .padding([.top, .leading], -10.0)
                        .padding(.trailing, -6.0)
                        .frame(width: logoWidth, height: logoHeight, alignment: .center)

//                    if gameState != .preview {
//                        Text(" \(home.score) ")
//                            .padding(2.0)
//                            .foregroundColor(gameScoreForeground)
//                            .background(gameScoreBackground)
//                            .font(Font.custom("HelveticaNeue-Bold", size: gameScoreFontSize))
//                            .cornerRadius(scoreCornerRadius)
//                    }
                }
            })
            .frame(maxWidth: .infinity, minHeight: scoreRowHeight, idealHeight: scoreRowHeight, maxHeight: scoreRowHeight, alignment: .center)
            
            // Game State text
            HStack {
                Spacer()
                Text(" \(self.game.status.abstractGameState.rawValue) ")
                .background(gameStateBackground)
                .font(Font.custom("HelveticaNeue-Bold", size: gameStateFontSize))
                .foregroundColor(gameStateForeground)
                .frame(height: gameStateRowHeight)
                Spacer()
            }
            .background(gameStateBackground)

        }) // VStack for entire row
        .background(Color(UIColor.systemGray5))
        .cornerRadius(8.0)
            .padding(-4.0)
    }
}
#endif

struct WatchScheduleGameRowView: View {
    var game : Game

    var body: some View {
        let away = game.teams.away
         let home = game.teams.home
         let logoHeight = CGFloat(50.0)
         let logoWidth = CGFloat(50.0)
         let atHeight = CGFloat(14.0)
         let atWidth = atHeight
         let scoreHeight = CGFloat(10.0)
         let scoreWidth = scoreHeight
         let scoreRowHeight = CGFloat(60.0)
        
         let gameStateFontSize = CGFloat(8.0)
         let gameStateRowHeight = CGFloat(12.0)
         
         var gameStateForeground : Color = Color.white
         var gameStateBackground : Color {
             switch(self.game.status.abstractGameState) {
             case .preview:
                 return Color(.blue)
             case .live:
                 return Color(.green)
             case .final:
                 return Color(.gray)
                 //Color.black
             }
         }
         var gameScoreFontSize = CGFloat(14.0)
         var gameScoreForeground : Color = Color.white
         var gameScoreBackground : Color {
             switch(self.game.status.abstractGameState) {
             case .preview:
                 return Color(.gray)
             case .live:
                 return Color(.gray)
             case .final:
                 return Color(.gray)
                 //Color.black
             }
         }

         return VStack(alignment: .center, spacing: 0.0, content: {
             HStack(alignment: .center, spacing: 4.0, content: {
                 ZStack(alignment: .bottomTrailing) {
                     Image(away.team.name)
                         .resizable()
                         .scaledToFit()
                         .padding(.top, -8.0)
                         .padding(.leading, -4.0)
                         .frame(width: logoWidth, height: logoHeight, alignment: .leading)

                     Text(" \(away.score) ")
                         .padding(2.0)
                         .foregroundColor(gameScoreForeground)
                         .background(gameScoreBackground)
                         .font(Font.custom("HelveticaNeue-Bold", size: gameScoreFontSize))
                         .cornerRadius(4.0)
                 }
                 //.background(Color(UIColor.systemGray2))

                 Image(systemName: "at")
                     .resizable()
                     .aspectRatio(contentMode:.fit)
                     .frame(width: atWidth, height: atHeight, alignment: .leading)

                 ZStack(alignment: .bottomLeading) {
                     Image(home.team.name)
                         .resizable()
                         .scaledToFit()
                         .padding(.top, -8.0)
                         .padding(.trailing, -4.0)
                         .frame(width: logoWidth, height: logoHeight, alignment: .leading)

                     Text(" \(home.score) ")
                         .padding(2.0)
                         .foregroundColor(gameScoreForeground)
                         .background(gameScoreBackground)
                         .font(Font.custom("HelveticaNeue-Bold", size: gameScoreFontSize))
                         .cornerRadius(4.0)
                 }
             })
             .frame(height: scoreRowHeight)
             
            // Game State text
            HStack {
                Spacer()
                Text(" \(self.game.status.abstractGameState.rawValue) ")
                .background(gameStateBackground)
                .font(Font.custom("HelveticaNeue-Bold", size: gameStateFontSize))
                .foregroundColor(gameStateForeground)
                .frame(height: gameStateRowHeight)
                Spacer()
            }
            .background(gameStateBackground)
        })
    }
}

@available(iOS 13.0, *)
struct ScheduleGameRowView_Preview: PreviewProvider {
  static var previews: some View {
    ForEach(deviceNames, id: \.self) { deviceName in
        ScheduleGameRowView(game: testGameFinal)
            .previewDevice(PreviewDevice(rawValue: deviceName))
            .previewDisplayName(deviceName)
        }
    }
}

//struct ScheduleGameRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        ScheduleGameRowView(game: testGameFinal)
//    }
//}

let testGameStatusFinal = GameStatus(abstractGameState: .final, codedGameState: "F", detailedState: "Final", statusCode: "F", startTimeTBD: false)
let testHomeTeam = Team(id: 14, name: "Penguins", link: "/team/14", venue: nil, teamName: "Penguins", locationName: "Pittsburgh", firstYearOfPlay: "1967", division: nil, conference: nil, franchise: nil, shortName: "PIT", officialSiteURL: "pittsburghpenguins.com", franchiseID: 14, active: true)
let testAwayTeam = Team(id: 25, name: "Lightning", link: "/team/25", venue: nil, teamName: "Lightning", locationName: "Tampa Bay", firstYearOfPlay: "1990", division: nil, conference: nil, franchise: nil, shortName: "TBL", officialSiteURL: "tampabaylightning.com", franchiseID: 25, active: true)
let testAwayLeagueRecord = LeagueRecord(wins: 34, losses: 16, ot: 4, type: "league")
let testHomeLeagueRecord = LeagueRecord(wins: 29, losses: 21, ot: 7, type: "league")
let testAwayTeamAndScore = GameTeamAndScore(leagueRecord: testAwayLeagueRecord, score: 3, team: testAwayTeam)
let testHomeTeamAndScore = GameTeamAndScore(leagueRecord: testHomeLeagueRecord, score: 2, team: testHomeTeam)
let testGameTeamsFinal = GameTeams(away: testAwayTeamAndScore, home: testHomeTeamAndScore)
let testVenue = Venue(id: 123, name: "PPG Paints Arena", link: "/venue/123", city: "Pittsburgh", timeZone: nil)

let testGameFinal = Game(gameId: 123456, link: "/game/feed/123456/", gameType: GameType(rawValue: "R")!, season: "20192020", gameDate: Date(), status: testGameStatusFinal, teams: testGameTeamsFinal, venue: testVenue, content: nil)

//public struct Game : Codable {
//    public let gameId: Int?
//    public let link : String?
//    public let gameType : GameType?
//    public let season: String?
//    public let gameDate: Date?
//    public let status: GameStatus?
//    public let teams: GameTeams?
//    public let venue: Venue?
//    public let content: GameContentLink?
//
//    public enum CodingKeys: String, CodingKey {
//        case gameId = "gamePk"
//        case link, gameType, season, gameDate, status, teams, venue, content
//     }
//}

//public struct GameStatus : Codable {
//    public let abstractGameState : GameStateType
//    public let codedGameState, detailedState, statusCode: String
//    public let startTimeTBD: Bool
//}

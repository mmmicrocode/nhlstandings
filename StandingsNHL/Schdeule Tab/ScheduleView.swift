//
//  ScheduleView.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/5/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI
import NHLAPI

#if os(watchOS)
typealias ScheduleView = WatchScheduleView
typealias DatePickerStyle = DefaultPickerStyle
#else
typealias ScheduleView = GenericScheduleView
typealias DatePickerStyle = SegmentedPickerStyle
#endif

#if !os(watchOS)
struct GenericScheduleView: View {
    @ObservedObject var scheduleVM = ScheduleViewModel()
    @State private var selectedDate : Int = 1
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    ForEach(self.scheduleVM.schedule.dates[self.selectedDate].games, id:\.gameId) { game in
                        NavigationLink(destination: ScheduleGameContentView(game: game)) {
                            ScheduleGameRowView(game: game)
                        }
                    }
                }
                .navigationBarTitle(self.scheduleVM.schedule.dates[self.selectedDate].date.nhlScheduleShortDisplayFormat)
            Picker("Date", selection: self.$selectedDate) {
                Text("Yesterday").tag(0)
                Text("Today").tag(1)
                Text("Tomorrow").tag(2)
            }.pickerStyle(DatePickerStyle())
                //.padding()
                //.background(Color.clear)
            }
        }
        .onAppear {
            print("ScheduleView.onAppear()")
            self.scheduleVM.reloadSchedule()
            UITableView.appearance().separatorStyle = .none
            UITableView.appearance().tableFooterView = UIView()
        }
        .onDisappear {
            UITableView.appearance().separatorStyle = .singleLine
            UITableView.appearance().tableFooterView = nil
        }
    }
}
#endif

struct WatchScheduleView: View {
    @ObservedObject var scheduleVM = ScheduleViewModel()
    @State private var selectedDate : Int = 1

//    var body: some View {
    var body: some View {
        let dayStrings = ["Yesterday", "Today", "Tomorrow"]

        return
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 0) {
                ForEach(0..<3) { num in
                    VStack {
                        Text("\(dayStrings[num])")
                            .font(.caption)

                        GeometryReader { geo in
                            List {
                                ForEach(self.scheduleVM.schedule.dates[num].games, id:\.gameId) { game in
                                    //NavigationLink(destination: ScheduleGameContentView(game: game)) {
                                        ScheduleGameRowView(game: game)
                                    //}
                                }
                            }

                            //.padding()
                            //.background(Color.red)
                            .rotation3DEffect(.degrees(-Double(geo.frame(in: .global).minX) / 8), axis: (x: 0, y: 1, z: 0))
                        }
                    }
                    .frame(width: 180)
                }
            }
            //.padding()
        }
        //.frame(height: 300)
        
        
//        VStack {
//            List {
//                ForEach(self.scheduleVM.schedule.dates[self.selectedDate].games, id:\.gameId) { game in
//                    NavigationLink(destination: ScheduleGameContentView(game: game)) {
//                        ScheduleGameRowView(game: game)
//                    }
//                }
//            }
//            .navigationBarTitle(self.scheduleVM.schedule.dates[self.selectedDate].date.nhlScheduleShortDisplayFormat)
//
//            Picker("Date", selection: self.$selectedDate) {
//                Text("Yesterday").tag(0)
//                Text("Today").tag(1)
//                Text("Tomorrow").tag(2)
//            }.pickerStyle(DatePickerStyle())
//                .frame(height: 40.0)
//                .padding([.bottom], 0.0)
//            //.background(Color.clear)
//        }
        .onAppear {
            print("ScheduleView.onAppear()")
            self.scheduleVM.reloadSchedule()
        }
    }
}

struct ScheduleView_Previews: PreviewProvider {
    static var previews: some View {
        ScheduleView()
    }
}

//
//  ConferenceTeamView.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 1/22/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI

struct GenericConferenceTeamView: View {
    var team : TeamRecordViewModel
    //@Binding var currentFavoriteTeam : Int
    
    var body: some View {
        NavigationLink(destination: TeamView(teamRecordVM: team)) {
            Image(team.teamName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 44.0, height: 44.0, alignment: .leading)
            VStack(alignment: .leading, spacing: 2.0) {
                Text(team.teamCity).bold()
                Text("PTS: \(team.record.points) (W: \(team.record.leagueRecord.wins) - L: \(team.record.leagueRecord.losses) - OT: \(team.record.leagueRecord.ot))")
            }
        }
        .padding(0.0)
    }
}

struct WatchConferenceTeamView: View {
    var team : TeamRecordViewModel
    //@Binding var currentFavoriteTeam : Int

    var body: some View {
        NavigationLink(destination: TeamView(teamRecordVM: team)) {
            Image(team.teamName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 44.0, height: 44.0, alignment: .leading)
            VStack(alignment: .leading, spacing: 2.0) {
                Text(team.teamCity)
                    .font(.headline)
                Text("\(team.record.points) pts")
                //\(team.record.leagueRecord.wins)W / \(team.record.leagueRecord.losses)L / \(team.record.leagueRecord.ot) OT")
                    .font(.subheadline)
            }
        }
        .padding(0.0)

    }
}

//struct GenericDivisionTeamView_Previews: PreviewProvider {
//    static var previews: some View {
//        GenericDivisionTeamView(rec: TeamRecordViewModel(record: testTeamRec1))
//    }
//}

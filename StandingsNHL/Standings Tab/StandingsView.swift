//
//  StandingsView.swift
//  Standings
//
//  Created by Heil, Steven on 11/14/19.
//  Copyright © 2019 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI

#if os(watchOS)
typealias StandingsView = WatchStandingsView
#else
typealias StandingsView = GenericStandingsView
#endif

#if !os(watchOS)
struct GenericStandingsView: View {
    var body: some View {
        NavigationView {
            StandingsList()
        }
    }
}
#endif

struct WatchStandingsView: View {
    var body: some View {
        StandingsList()
    }
}

struct StandingsView_Previews: PreviewProvider {
    static var previews: some View {
        StandingsView()
    }
}

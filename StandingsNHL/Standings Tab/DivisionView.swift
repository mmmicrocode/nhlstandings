//
//  DivisionView.swift
//  Standings
//
//  Created by Heil, Steven on 11/14/19.
//  Copyright © 2019 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI
//import NHLAPI


#if os(watchOS)
struct DivisionView: View {
    var divisionRecordVM : DivisionRecordViewModel

    var body: some View {
        List(self.divisionRecordVM.teamRecords) { rec in
            DivisionTeamView(team: rec)
        }
        .navigationBarTitle("\(divisionRecordVM.divisionName)")
    }
}
#else
struct DivisionView: View {
    @ObservedObject var userDefaultsManager = UserDefaultsManager()
    var divisionRecordVM : DivisionRecordViewModel

    var body: some View {
        List(self.divisionRecordVM.teamRecords) { rec in
            DivisionTeamView(team: rec)
        }
        .navigationBarTitle("\(self.divisionRecordVM.divisionName)")
            
        .navigationBarItems(trailing:
            Image(systemName: self.$userDefaultsManager.favoriteDivision.wrappedValue == self.divisionRecordVM.id ? "star.fill" : "star")
                .onTapGesture {
                    self.$userDefaultsManager.favoriteDivision.wrappedValue = (self.$userDefaultsManager.favoriteDivision.wrappedValue == self.divisionRecordVM.id ? 0 : self.divisionRecordVM.id)
                }
        )
    }
}
#endif

#if os(watchOS)
typealias DivisionTeamView = WatchDivisionTeamView
#else
typealias DivisionTeamView = GenericDivisionTeamView
#endif

//struct DivisionView_Previews: PreviewProvider {
//    static var previews: some View {
//        DivisionView(divisionRecordVM: testDivRec)
//    }
//}

//let testDivRec = DivisionRecordViewModel(record: )

//let testTeamRec1 = TeamRecord(team: Team(id: 5, name: "Pittsburgh Penguins", link: "/link"), leagueRecord: LeagueRecord(wins: 5, losses: 2, ot: 0, type: .league), goalsAgainst: 18, goalsScored: 28, points: 10, divisionRank: "2", divisionL10Rank: "2", divisionRoadRank: "3", divisionHomeRank: "2", conferenceRank: "4", conferenceL10Rank: "4", conferenceRoadRank: "3", conferenceHomeRank: "2", leagueRank: "7", leagueL10Rank: "7", leagueRoadRank: "8", leagueHomeRank: "8", wildCardRank: "0", row: 5, gamesPlayed: 7, streak: Streak(streakType: .wins, streakNumber: 4, streakCode: "W4"), lastUpdated: "2019-10-17T01:35:06Z")
//
//let testDivRec = DivisionRecordViewModel(record: Record(standingsType: .regularSeason, league: Simple(id: 1, name: "Metropolitan", link: ""), division: Expanded(id: 1, name: "Metropolitan", nameShort: "Metro", link: "", abbreviation: "MET"), conference: Simple(id: 1, name: "Eastern", link: ""), teamRecords: [ testTeamRec1 ]))

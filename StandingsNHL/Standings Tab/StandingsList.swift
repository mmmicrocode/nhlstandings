//
//  StandingsList.swift
//  Standings
//
//  Created by Heil, Steven on 11/14/19.
//  Copyright © 2019 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI
//import NHLAPI

enum StandingsTypeSelection : Int {
    case Division = 0
    case Conference = 1
    case WildCard = 2
}

struct StandingsList: View {
    @ObservedObject var standingsVM = StandingsViewModel()
    @State var networkIndicator = false
    @State var standingsType : StandingsTypeSelection = .WildCard
    
    var navBarTitle : String {
        switch(self.standingsType) {
        case .WildCard:
            return "Wild Card"
        case .Conference:
            return "Conference"
        default:
            return "Division"
        }
    }
    
    var body: some View {
    VStack() {
        if self.standingsVM.conferenceRecords.isEmpty
            && self.standingsVM.divisionRecords.isEmpty
            && self.standingsVM.wildCardRecords.isEmpty {
            Text("Loading current standings...").foregroundColor(.gray)
        }
        else {
            if self.standingsType == StandingsTypeSelection.Division {
                List {
                    ForEach(self.standingsVM.divisionRecords) { divRec in
                    DivisionStandingsRow(divisionRecord: divRec)
                    }

                    Text("\(self.standingsVM.timestampString)")
                            .foregroundColor(.secondary)
                            .font(.caption)
                }
                //.listStyle(GroupedListStyle())
            }
            else if self.standingsType == StandingsTypeSelection.Conference {
                List {
                    ForEach(self.standingsVM.conferenceRecords) { confRec in
                    ConferenceStandingsRow(conferenceRecord: confRec)
                    }

                    Text("\(self.standingsVM.timestampString)")
                        .foregroundColor(.secondary)
                        .font(.caption)
                }
                //.listStyle(GroupedListStyle())
            }
            else {
                List {
                    ForEach(self.standingsVM.wildCardRecords) { wcRec in
                        ConferenceWildCardStandingsRow(conferenceWildCardRecord: wcRec)
                    }
                    Text("\(self.standingsVM.timestampString)")
                        .foregroundColor(.secondary)
                        .font(.caption)
                }
                //.listStyle(GroupedListStyle())
            }
        }
    }
          .navigationBarTitle(navBarTitle)
        
        .contextMenu(menuItems: {
            Button(action: {
                switch(self.standingsType) {
                case .Conference:
                    self.standingsVM.reloadConferenceStandings()
                case .WildCard:
                    self.standingsVM.reloadWildCardStandings()
                case .Division:
                    self.standingsVM.reloadDivisionStandings()
                }
            }, label: {
                VStack{
                    Image(systemName: "arrow.clockwise")
                        .font(.title)
                    Text("Reload")
                }
            })
            Button(action: {
                self.standingsType = .Conference
                self.standingsVM.reloadConferenceStandings()
            }, label: {
                VStack{
                    Image(systemName: "list.number")
                        .font(.title)
                    Text("Conference")
                }
            })
            Button(action: {
                self.standingsType = .Division
                self.standingsVM.reloadDivisionStandings()
            }, label: {
                VStack{
                    Image(systemName: "list.number")
                        .font(.title)
                    Text("Division")
                }
            })
            Button(action: {
                self.standingsType = .WildCard
                self.standingsVM.reloadWildCardStandings()
            }, label: {
                VStack{
                    Image(systemName: "list.number")
                        .font(.title)
                    Text("Wild Card")
                }
            })
        })

//        .onAppear {
//            //UITableView.appearance().separatorStyle = .none
//            UITableView.appearance().tableFooterView = UIView()
//        }
//        .onDisappear {
//            //UITableView.appearance().separatorStyle = .singleLine
//            UITableView.appearance().tableFooterView = nil
//        }

//        .disabled(networkIndicator)
//        .blur(radius: networkIndicator ? 1.0 : 0.0)
//        .onAppear {
//            self.networkIndicator = true
//        }
    }
}

struct StandingsList_Previews: PreviewProvider {
    static var previews: some View {
        StandingsList()
    }
}

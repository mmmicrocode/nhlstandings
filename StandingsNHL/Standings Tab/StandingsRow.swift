//
//  StandingsRow.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 1/22/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI

#if os(watchOS)
typealias DivisionStandingsRow = WatchDivisionStandingsRow
typealias ConferenceStandingsRow = WatchConferenceStandingsRow
typealias ConferenceWildCardStandingsRow = WatchConferenceWildCardStandingsRow
#else
typealias DivisionStandingsRow = GenericDivisionStandingsRow
typealias ConferenceStandingsRow = GenericConferenceStandingsRow
typealias ConferenceWildCardStandingsRow = GenericConferenceWildCardStandingsRow
#endif

struct GenericDivisionStandingsRow: View {
    var divisionRecord : DivisionRecordViewModel
    
    var body: some View {
        NavigationLink(destination: DivisionView(divisionRecordVM: divisionRecord)) {
            Image(divisionRecord.divisionName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40.0, height: 40.0, alignment: .leading)
            Text(divisionRecord.divisionName).bold()
            Spacer()
//            Image(systemName: currentFavoriteDivision == divisionRecord.division?.id ? "star.fill" : "star")
//                .onTapGesture {
//                    self.currentFavoriteDivision = (self.currentFavoriteDivision == self.divisionRecord.division?.id ? 0 : self.divisionRecord.division?.id)
//                }
        }
//        .onTapGesture {
//            self.currentSelectedDivision = self.divisionRecord.division?.id ?? 0
//        }
    }
}

struct GenericConferenceStandingsRow: View {
    var conferenceRecord : ConferenceRecordViewModel
    
    var body: some View {
        NavigationLink(destination: ConferenceView(conferenceRecordVM: conferenceRecord)) {
            Image(conferenceRecord.conferenceName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40.0, height: 40.0, alignment: .leading)
            Text(conferenceRecord.conferenceName)
                .bold()
            Spacer()
//            Image(systemName: currentFavoriteConference == conferenceRecord.conference?.id ? "star.fill" : "star")
//                .onTapGesture {
//                    self.currentFavoriteDivision = (self.currentFavoriteDivision == self.divisionRecord.division.id ? 0 : self.divisionRecord.division.id)
//                }
        }
//        .onTapGesture {
//            self.currentSelectedConference = self.conferenceRecord.conference?.id ?? 0
//        }
    }
}

struct GenericConferenceWildCardStandingsRow: View {
    var conferenceWildCardRecord : WildCardConferenceRecordViewModel
    
    var body: some View {
        NavigationLink(destination: ConferenceWildCardView(conferenceWildCardVM: conferenceWildCardRecord)) {
            Image(conferenceWildCardRecord.conferenceName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40.0, height: 40.0, alignment: .leading)
            Text(conferenceWildCardRecord.conferenceName)
                .bold()
            Spacer()
        }
    }
}


struct WatchDivisionStandingsRow: View {
    var divisionRecord : DivisionRecordViewModel
    
    var body: some View {
        NavigationLink(destination: DivisionView(divisionRecordVM: divisionRecord)) {                Image(divisionRecord.divisionName)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 35.0, height: 35.0, alignment: .leading)
                Text(divisionRecord.divisionName)
                    .bold()
                    //.font(.caption)
            }
    }
}

struct WatchConferenceStandingsRow: View {
    var conferenceRecord : ConferenceRecordViewModel
    
    var body: some View {
        NavigationLink(destination: ConferenceView(conferenceRecordVM: conferenceRecord)) {                Image(conferenceRecord.conferenceName)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 35.0, height: 35.0, alignment: .leading)
                Text(conferenceRecord.conferenceName)
                    .bold()
                    //.font(.caption)
            }
    }
}

struct WatchConferenceWildCardStandingsRow: View {
    var conferenceWildCardRecord : WildCardConferenceRecordViewModel
    
    var body: some View {
        NavigationLink(destination: ConferenceWildCardView(conferenceWildCardVM: conferenceWildCardRecord)) {
            Image(conferenceWildCardRecord.conferenceName)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 35.0, height: 35.0, alignment: .leading)
                Text(conferenceWildCardRecord.conferenceName)
                    .bold()
                    //.font(.caption)
            }
    }
}

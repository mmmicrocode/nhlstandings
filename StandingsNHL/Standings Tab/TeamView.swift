//
//  TeamView.swift
//  Standings WatchKit Extension
//
//  Created by Heil, Steven on 10/21/19.
//  Copyright © 2019 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI
import NHLAPI

struct TeamView: View {
    @ObservedObject var teamDetailVM = TeamDetailViewModel()
    @State var playerCardPresented = false
    var teamRecordVM : TeamRecordViewModel
    
    var body: some View {
//        ScrollView {
            VStack(alignment: .center, spacing: 0.0, content: {
                Image(teamRecordVM.teamName)
                    .resizable()
//                    .renderingMode(.original)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 120, height: 100)
                    //.scaledToFit()

                Text(teamRecordVM.teamName).font(.subheadline).lineLimit(0)
                //Divider()
                Spacer()
                HStack(alignment: .center, spacing: 8.0) {
                    VStack {
                        Text("GP").foregroundColor(.secondary)
                        Text("\(teamRecordVM.record.gamesPlayed)")
                    }
                    VStack {
                        Text("PT").foregroundColor(.secondary)
                        Text("\(teamRecordVM.record.points)")
                    }
                    VStack {
                        Text("W").foregroundColor(.secondary)
                        Text("\(teamRecordVM.record.leagueRecord.wins)")
                    }
                    VStack {
                        Text("L").foregroundColor(.secondary)
                        Text("\(teamRecordVM.record.leagueRecord.losses)")
                    }
                    VStack {
                        Text("OT").foregroundColor(.secondary)
                        Text("\(teamRecordVM.record.leagueRecord.ot)")
                    }
                    VStack {
                        Text("STK").foregroundColor(.secondary)
                        Text("\(teamRecordVM.record.streak.streakCode)")
                    }
                }
                //.frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 60.0, alignment: Alignment.center)
                //.background(Color(.systemGroupedBackground))
                .cornerRadius(8.0)
                .padding(4.0)
                
                Spacer()
                
                if self.teamDetailVM.teamRoster != nil {
                    List {
                        ForEach(self.teamDetailVM.teamRoster!.rosterSortedByNumber, id: \RosterElement.person.id) { rosterEl in
                            RosterRow(cardPresented: self.$playerCardPresented, rosterSpot: rosterEl)
                        }
                    }
                }

            })
                //.padding(0.0)

                .navigationBarTitle(teamRecordVM.teamCity)
                .onAppear(perform: fetchTeamRoster)
        
    }
    
//    private func fetchTeamDetail() {
//        self.teamDetailVM.reloadTeamDetails(id: self.teamRecordVM.id)
//    }
    private func fetchTeamRoster() {
        self.teamDetailVM.reloadTeamRoster(id: self.teamRecordVM.id)
    }
}

#if os(watchOS)
typealias RosterRow = WatchRosterRow
#else
typealias RosterRow = GenericRosterRow
#endif

struct WatchRosterRow : View {
    @Binding var cardPresented : Bool
    var rosterSpot : RosterElement
    
    var body: some View {
        //HStack {
        NavigationLink(destination: PlayerCardView(rosterSpot: rosterSpot, onDismiss: { } )) {
            Text(rosterSpot.jerseyNumber)
            Spacer()
            Text(rosterSpot.position.code)
            Spacer()
            Text(rosterSpot.person.fullName)
            
//            Button(action: { self.cardPresented = true }) {
//                Text("")
//            }.sheet(isPresented: $cardPresented) {
//                PlayerCardView(rosterSpot: self.rosterSpot, onDismiss: {
//                    self.cardPresented = false
//                })
//            }

        }
        .onTapGesture {
            self.$cardPresented.wrappedValue = true
        }
    }
}

struct GenericRosterRow : View {
    @Binding var cardPresented : Bool
    var rosterSpot : RosterElement
    
    var body: some View {
//        ModalPresenter {
//        ModalLink(destination: Text("Card")) {
//            PlayerCardView(rosterSpot: rosterSpot, onDismiss: {} )
//        }
        
//        HStack {

        NavigationLink(destination: PlayerCardView(rosterSpot: rosterSpot, onDismiss: { } )) {
            Text(rosterSpot.jerseyNumber)
            Spacer()
            Text(rosterSpot.position.code)
            Spacer()
            Text(rosterSpot.person.fullName)
            
//            Button(action: { self.cardPresented = true }) {
//                Text("")
//            }.sheet(isPresented: $cardPresented) {
//                PlayerCardView(rosterSpot: self.rosterSpot, onDismiss: {
//                    self.cardPresented = false
//                })
//            }

        }
//        .onTapGesture {
//            self.$cardPresented.wrappedValue = true
//        }
    }
}

//struct TeamView_Previews: PreviewProvider {
//    static var previews: some View {
//        TeamView(teamRecordVM: TeamRecordViewModel(record: teamRec1))
//    }
//}

//let teamRec1 = TeamRecord(team: Team(id: 5, name: "Pittsburgh Penguins", link: "", venue: nil, teamName: nil, locationName: nil, firstYearOfPlay: nil, division: nil, conference: nil, franchise: nil, shortName: nil, officialSiteURL: nil, franchiseID: nil, active: true), leagueRecord: LeagueRecord(wins: 14, losses: 10, ot: 4, type: LeagueRecordType.league), goalsAgainst: 78, goalsScored: 90, points: 26, divisionRank: "4", divisionL10Rank: "5", divisionRoadRank: "6", divisionHomeRank: "4", conferenceRank: "7", conferenceL10Rank: "8", conferenceRoadRank: "8", conferenceHomeRank: "8", leagueRank: "12", leagueL10Rank: "12", leagueRoadRank: "12", leagueHomeRank: "12", wildCardRank: "14", row: 14, gamesPlayed: 24, streak: Streak(streakType: StreakType.wins, streakNumber: 2, streakCode: "W2"), lastUpdated: Date())

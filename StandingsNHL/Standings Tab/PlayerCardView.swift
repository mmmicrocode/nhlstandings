//
//  PlayerCardView.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 1/31/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI
import NHLAPI

struct PlayerCardView : View {
    var rosterSpot : RosterElement
    var onDismiss: () -> ()
    @ObservedObject private var playerCardVM = PlayerCardViewModel()
    let currentSeason = "20192020"
    
    var body: some View {
        VStack {
            Text(self.rosterSpot.person.fullName)
            
            // Person Details
            if(self.playerCardVM.person != nil) {
                Text(self.playerCardVM.person!.people[0].nationality)
            }
            else {
                Text("---")
            }
            
            // Person Stats - Current Single Season
            if(self.playerCardVM.personStats != nil) {
                Text(self.playerCardVM.personStats!.stats[0].splits[0].stat.timeOnIce)
            }
            else {
                Text("---")
            }
            
//            Button(action: { self.onDismiss() }) {
//                Text("Dismiss")
//            }
        }
        .onAppear {
            self.fetchPersonDetails()
            self.fetchPersonStatsSingleSeason()
        }
    }
    
    private func fetchPersonDetails() {
        self.playerCardVM.reloadPersonDetail(personId: self.rosterSpot.person.id)
    }
    private func fetchPersonStatsSingleSeason() {
        self.playerCardVM.reloadPersonStatsSingleSeason(personId: self.rosterSpot.person.id, season: self.currentSeason)
    }
}

let deviceNames: [String] = [
    "iPhone SE",
    "iPhone 11 Pro"
//    "iPad Pro (11-inch)"
]

@available(iOS 13.0, *)
struct PlayerCardView_Preview: PreviewProvider {
  static var previews: some View {
    ForEach(deviceNames, id: \.self) { deviceName in
        PlayerCardView(rosterSpot: testRosterElement1, onDismiss: {})
            .previewDevice(PreviewDevice(rawValue: deviceName))
            .previewDisplayName(deviceName)
        }
    }
}

//struct PlayerCardView_Previews: PreviewProvider {
//    static var previews: some View {
//        PlayerCardView(rosterSpot: testRosterElement1, onDismiss: {})
//        .previewDevice(PreviewDevice(rawValue: "iPhone 11"))
//        .previewDisplayName("iPhone 11")
//    }
//}

let testPositionDefense = Position(code: "D", name: "Defenseman", type: PositionType.defenseman, abbreviation: "D")
let testPositionCenter = Position(code: "C", name: "Center", type: PositionType.forward, abbreviation: "C")
let testPositionLeftWing = Position(code: "LW", name: "Left Wing", type: PositionType.forward, abbreviation: "LW")
let testPositionRightWing = Position(code: "RW", name: "Right Wing", type: PositionType.forward, abbreviation: "RW")
let testPositionGoalie = Position(code: "G", name: "Goalie", type: PositionType.goalie, abbreviation: "G")

let testPerson1 = RosterPerson(id: 8474176, fullName: "Carl Hagelin", link: "/api/v1/people/8474176")
let testJerseyNum1 = "62"
let testPerson2 = RosterPerson(id: 8471214, fullName: "Alex Ovechkin", link: "/api/v1/people/8471214")
let testJerseyNum2 = "8"
let testPerson3 = RosterPerson(id: 8471698, fullName: "T.J. Oshie", link: "/api/v1/people/8471698")
let testJerseyNum3 = "77"
let testPerson4 = RosterPerson(id: 8474651, fullName: "Braden Holtby", link: "/api/v1/people/8474651")
let testJerseyNum4 = "70"

let testRosterElement1 = RosterElement(person: testPerson1, jerseyNum: testJerseyNum1, position: testPositionLeftWing)
let testRosterElement2 = RosterElement(person: testPerson2, jerseyNum: testJerseyNum2, position: testPositionCenter)
let testRosterElement3 = RosterElement(person: testPerson3, jerseyNum: testJerseyNum3, position: testPositionRightWing)
let testRosterElement4 = RosterElement(person: testPerson4, jerseyNum: testJerseyNum4, position: testPositionGoalie)


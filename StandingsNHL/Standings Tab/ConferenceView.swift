//
//  ConferenceView.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 1/22/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI
//import NHLAPI

#if os(watchOS)
struct ConferenceView: View {
    var conferenceRecordVM : ConferenceRecordViewModel
    @State var selectedTeam : Int = 0
    @State var favoriteTeam : Int = 0
    
    var body: some View {
        List(self.conferenceRecordVM.teamRecords) { rec in
            ConferenceTeamView(team: rec)
        }
        .navigationBarTitle("\(conferenceRecordVM.conferenceName)")
    }
}
#else
struct ConferenceView: View {
    var conferenceRecordVM : ConferenceRecordViewModel
    @State var selectedTeam : Int = 0
    @State var favoriteTeam : Int = 0
    
    var body: some View {
        List(self.conferenceRecordVM.teamRecords) { rec in
            ConferenceTeamView(team: rec)
        }
        .navigationBarTitle("\(conferenceRecordVM.conferenceName)")
    }
}
#endif

#if os(watchOS)
typealias ConferenceTeamView = WatchConferenceTeamView
#else
typealias ConferenceTeamView = GenericConferenceTeamView
#endif

//struct DivisionView_Previews: PreviewProvider {
//    static var previews: some View {
//        DivisionView(divisionRecordVM: testDivRec)
//    }
//}

//let testDivRec = DivisionRecordViewModel(record: )

//let testTeamRec1 = TeamRecord(team: Team(id: 5, name: "Pittsburgh Penguins", link: "/link"), leagueRecord: LeagueRecord(wins: 5, losses: 2, ot: 0, type: .league), goalsAgainst: 18, goalsScored: 28, points: 10, divisionRank: "2", divisionL10Rank: "2", divisionRoadRank: "3", divisionHomeRank: "2", conferenceRank: "4", conferenceL10Rank: "4", conferenceRoadRank: "3", conferenceHomeRank: "2", leagueRank: "7", leagueL10Rank: "7", leagueRoadRank: "8", leagueHomeRank: "8", wildCardRank: "0", row: 5, gamesPlayed: 7, streak: Streak(streakType: .wins, streakNumber: 4, streakCode: "W4"), lastUpdated: "2019-10-17T01:35:06Z")
//
//let testDivRec = DivisionRecordViewModel(record: Record(standingsType: .regularSeason, league: Simple(id: 1, name: "Metropolitan", link: ""), division: Expanded(id: 1, name: "Metropolitan", nameShort: "Metro", link: "", abbreviation: "MET"), conference: Simple(id: 1, name: "Eastern", link: ""), teamRecords: [ testTeamRec1 ]))

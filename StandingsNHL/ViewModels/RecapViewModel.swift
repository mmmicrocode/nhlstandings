//
//  RecapViewModel.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/11/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import UIKit
import Combine
import NHLAPI

class RecapViewModel : ObservableObject, Identifiable {
    var recapItem : ArticlesItem
    @Published var recapImageUrlString : String?
    @Published var recapVideoUrlString : String?
    
    var id : String {
        return recapItem.id ?? "-1"
    }
    
    init(recapItem : ArticlesItem) {
        self.recapItem = recapItem
        
        if let image = self.recapItem.image {
            if let cuts = image.cuts, let medImg = cuts["568x320"], let imgSrc = medImg.src {
                self.recapImageUrlString = imgSrc
//                if let imgUrl = URL(string: imgSrc) {
//                    self.recapImageUrl = imgUrl
//                }
            }
        }
        
        if let pb = self.recapItem.playbacks {
            let mobilePlayback = pb.filter { $0.name == "HTTP_CLOUD_MOBILE" }
            if mobilePlayback.count > 0 {
                self.recapVideoUrlString = mobilePlayback[0].url
//                if let srcUrl = URL(string: mobilePlayback[0].url) {
//                    self.recapVideoUrl = srcUrl
//                }
            }
        }
    }
}

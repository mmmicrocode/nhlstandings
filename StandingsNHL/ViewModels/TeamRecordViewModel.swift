//
//  TeamRecordViewModel.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 1/22/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import Foundation
import NHLAPI

class TeamRecordViewModel : ObservableObject, Identifiable {
    var record : TeamRecord

    var id: Int {
        return self.record.team.id
    }

    var teamName: String { return self.record.team.name }
    var teamNickname: String { return self.record.team.nickname }
    var teamCity: String { return self.record.team.city }
//    var teamImage: UIImage? { return self.record.team.image ?? nil }
//    var teamCGImage: CGImage? { return self.record.team.cgimage }

//    var divisionRank : String { return self.record.divisionRank }

    init(record: TeamRecord) {
        self.record = record
    }
    
//    var teamName: String?
    var goalsAgainst, goalsScored, points: Int?
    var divisionRank, divisionL10Rank, divisionRoadRank, divisionHomeRank: String?
    var conferenceRank, conferenceL10Rank, conferenceRoadRank, conferenceHomeRank: String?
    var leagueRank, leagueL10Rank, leagueRoadRank, leagueHomeRank: String?
    var wildCardRank: String?
    var row, gamesPlayed: Int?
    var lastUpdated: String?
}

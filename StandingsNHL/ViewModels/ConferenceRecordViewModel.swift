//
//  ConferenceRecordViewModel.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 1/22/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import Foundation
import NHLAPI

class ConferenceRecordViewModel : ObservableObject, Identifiable {
    var standingsType: String?
    var record : Record
    var league : Simple
    var conference : Simple?
    var teamRecords = [TeamRecordViewModel]()

    var id: Int {
        return self.conference?.id ?? 0
    }
    var conferenceName: String {
        return self.conference?.name ?? "n/a"
    }
    
    init(record: Record) {
        self.record = record
        self.league = self.record.league
        self.conference = self.record.conference
        self.teamRecords = self.record.teamRecords.map { TeamRecordViewModel.init(record: $0) }
    }
}

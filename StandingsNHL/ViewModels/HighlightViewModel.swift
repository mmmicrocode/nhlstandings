//
//  HighlightViewModel.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/11/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import UIKit
import Combine
import NHLAPI

class HighlightViewModel : ObservableObject, Identifiable {
    var highlightItem : ArticlesItem
    @Published var highlightVideoUrlString : String = ""
    @Published var highlightTitle : String = ""
        
    var id : String {
        return highlightItem.id ?? "-1"
    }
    
    init(highlightItem : ArticlesItem) {
        self.highlightItem = highlightItem
        self.highlightTitle = self.highlightItem.title ?? ""
        
        if let pb = self.highlightItem.playbacks {
            let mobilePlayback = pb.filter { $0.name == "HTTP_CLOUD_MOBILE" }
            if mobilePlayback.count > 0 {
                self.highlightVideoUrlString = mobilePlayback[0].url
                
               //if let srcUrl = URL(string: mobilePlayback[0].url) {
               //    self.highlightsVideoUrl = srcUrl
               //}
           }
       }
   }
}

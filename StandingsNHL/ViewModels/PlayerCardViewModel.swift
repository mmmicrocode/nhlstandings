//
//  PlayerCardViewModel.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/1/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import Foundation
import Combine
import NHLAPI

class PlayerCardViewModel : ObservableObject, Identifiable {
    @Published var person : Person? = nil
    @Published var personStats : IndividualStats? = nil

    //let currentSeason = "20192020"
    
    var personDetailActivitySubscriber: AnyCancellable?
    var personDetailDataSubscriber: AnyCancellable?
    var personStatsSingleSeasonActivitySubscriber: AnyCancellable?
    var personStatsSingleSeasonDataSubscriber: AnyCancellable?
    
    func reloadPersonDetail(personId: Int) {
        let apiActivitySub = NHLAPI.nhlPersonDetailsActivityPublisher
        .receive(on: RunLoop.main)
            .sink { doingSomethingNow in
                if (doingSomethingNow) {
                    print("start personDetail activity...")
                } else {
                    print("stop personDetail activity...")
                }
        }
        if personDetailActivitySubscriber == nil {
            personDetailActivitySubscriber = AnyCancellable(apiActivitySub)
        }
        
        personDetailDataSubscriber = NHLAPI.nhlPersonDetailsDataPublisher(personId: personId)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
            switch completion {
            case .failure(let error):
                print(error)
            case .finished:
                print("DONE - personDetail postDataPublisher")
                //self.divisionRecords = self.localDivisionRecords
            }
        }, receiveValue: { (persons) in
            if persons.count > 0 {
                self.person = persons[0]
            }
        })
    }

    func reloadPersonStatsSingleSeason(personId: Int, season: String) {
        let apiActivitySub = NHLAPI.nhlPersonStatsSingleSeasonActivityPublisher
        .receive(on: RunLoop.main)
            .sink { doingSomethingNow in
                if (doingSomethingNow) {
                    print("start personStatsSingleSeason activity...")
                } else {
                    print("stop personStatsSingleSeason activity...")
                }
        }
        if personStatsSingleSeasonActivitySubscriber == nil {
            personStatsSingleSeasonActivitySubscriber = AnyCancellable(apiActivitySub)
        }
        
        personStatsSingleSeasonDataSubscriber = NHLAPI.nhlPersonStatsSingleSeasonDataPublisher(personId: personId, season: season)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
            switch completion {
            case .failure(let error):
                print(error)
            case .finished:
                print("DONE - personStatsSingleSeason postDataPublisher")
            }
        }, receiveValue: { (stats) in
            self.personStats = stats
        })
    }

    init() {
        //reloadTeamDetails()
    }
}

//
//  StandingsViewModel.swift
//  Standings WatchKit Extension
//
//  Created by Heil, Steven on 10/18/19.
//  Copyright © 2019 Thermo Fisher Scientific. All rights reserved.
//

import UIKit
import UserNotifications
import Combine
import NHLAPI

class StandingsViewModel : ObservableObject {
    @Published var divisionRecords = [DivisionRecordViewModel]()
    @Published var conferenceRecords = [ConferenceRecordViewModel]()
    @Published var wildCardRecords = [WildCardConferenceRecordViewModel]()
    @Published var timestamp : Date? = nil

    // 4 total, 1 for each division
    var localDivisionRecords = [DivisionRecordViewModel]()
    // 2 total, 1 for each conference
    var localConferenceRecords = [ConferenceRecordViewModel]()
    // 2 total, 1 for each conference, with wildCard + 2 divisionLeader teamRecords arrays in each
    var localWildCardRecords = [WildCardConferenceRecordViewModel]()
    
    var nhlStandingsByDivisionActivitySubscriber: AnyCancellable?
    var nhlStandingsByDivisionDataSubscriber: AnyCancellable?
    
    var nhlStandingsByConferenceActivitySubscriber: AnyCancellable?
    var nhlStandingsByConferenceDataSubscriber: AnyCancellable?

    var nhlStandingsByWildCardActivitySubscriber: AnyCancellable?
    var nhlStandingsByWildCardDataSubscriber: AnyCancellable?

    var timestampString : String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yy hh:mma"
        guard let time = self.timestamp else { return "" }
        return formatter.string(from: time)
    }
    
    func reloadDivisionStandings() {
        let apiActivitySub = NHLAPI.nhlStandingsByDivisionActivityPublisher
        .receive(on: RunLoop.main)
            .sink { doingSomethingNow in
                if (doingSomethingNow) {
                    //self.activityIndicator.startAnimating()
                    print("start byDivision activity...")
                } else {
                    print("stop byDivision activity...")
                }
        }
        if nhlStandingsByDivisionActivitySubscriber == nil {
            nhlStandingsByDivisionActivitySubscriber = AnyCancellable(apiActivitySub)
        }
        
        nhlStandingsByDivisionDataSubscriber = NHLAPI.nhlStandingsByDivisionDataPublisher
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
            switch completion {
            case .failure(let error):
                print(error)
            case .finished:
                print("DONE - byDivision postDataPublisher")
                self.timestamp = Date()
                self.divisionRecords = self.localDivisionRecords
            }
        }, receiveValue: { (standings) in
            self.localDivisionRecords = standings.records.map { DivisionRecordViewModel(record: $0) }
        })
    }

    func reloadConferenceStandings() {
        let apiActivitySub = NHLAPI.nhlStandingsByConferenceActivityPublisher
        .receive(on: RunLoop.main)
            .sink { doingSomethingNow in
                if (doingSomethingNow) {
                    //self.activityIndicator.startAnimating()
                    print("start byConference activity...")
                } else {
                    print("stop byConference activity...")
                }
        }
        if nhlStandingsByConferenceActivitySubscriber == nil {
            nhlStandingsByConferenceActivitySubscriber = AnyCancellable(apiActivitySub)
        }
        
        nhlStandingsByConferenceDataSubscriber = NHLAPI.nhlStandingsByConferenceDataPublisher
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
            switch completion {
            case .failure(let error):
                print(error)
            case .finished:
                print("DONE - byConference postDataPublisher")
                self.timestamp = Date()
                self.conferenceRecords = self.localConferenceRecords
            }
        }, receiveValue: { (standings) in
            self.localConferenceRecords = standings.records.map { ConferenceRecordViewModel(record: $0) }
        })
    }

    func reloadWildCardStandings() {
        let apiActivitySub = NHLAPI.nhlStandingsByWildCardActivityPublisher
        .receive(on: RunLoop.main)
            .sink { doingSomethingNow in
                if (doingSomethingNow) {
                    //self.activityIndicator.startAnimating()
                    print("start byWildCard activity...")
                } else {
                    print("stop byWildCard activity...")
                }
        }
        if nhlStandingsByWildCardActivitySubscriber == nil {
            nhlStandingsByWildCardActivitySubscriber = AnyCancellable(apiActivitySub)
        }
        
        nhlStandingsByWildCardDataSubscriber = NHLAPI.nhlStandingsByWildCardDataPublisher
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
            switch completion {
            case .failure(let error):
                print(error)
            case .finished:
                print("DONE - byWildCard postDataPublisher")
                self.timestamp = Date()
                self.wildCardRecords = self.localWildCardRecords
            }
        }, receiveValue: { (standings) in
            //self.localWildCardRecords = standings.records.map { WildCardRecordViewModel(record: $0) }
            self.localWildCardRecords.removeAll()
            
            // We will 'hardcode' our filtering of the 2 conferences
            self.localWildCardRecords.append(WildCardConferenceRecordViewModel(records: standings.records.filter( {
                $0.conference?.name == "Eastern"
            })))
            
            self.localWildCardRecords.append(WildCardConferenceRecordViewModel(records: standings.records.filter( {
                $0.conference?.name == "Western"
            })))

        })
    }

    init() {
//        reloadDivisionStandings()
        
   //     reloadConferenceStandings()
        
        reloadWildCardStandings()
        
//        nhlStandingsDataSubscriber = NHLAPI.nhlStandingsDataPublisher
//            .receive(on: RunLoop.main)
//
//            //.sink()
////            .receive(on: RunLoop.main)
//            .assign(to: \.divisionRecords, on: self)
////        self.divisionRecords = self.standings.map { DivisionRecordViewModel.init(record: $0) }
    }
}

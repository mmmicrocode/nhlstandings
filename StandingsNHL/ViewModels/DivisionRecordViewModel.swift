//
//  DivisionRecordViewModel.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 1/22/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import Foundation
import NHLAPI

class DivisionRecordViewModel : ObservableObject, Identifiable {
    var standingsType: String?
    var record : Record
    var league : Simple
    var division : Expanded?
    var conference : Simple?
    var teamRecords = [TeamRecordViewModel]()

    var id: Int {
        return self.division?.id ?? 0
    }
    var divisionName: String {
        return self.division?.name ?? "n/a"
    }

    init(record: Record) {
        self.record = record

        self.league = self.record.league
        self.division = self.record.division
        self.conference = self.record.conference
        self.teamRecords = self.record.teamRecords.map { TeamRecordViewModel.init(record: $0) }
    }
}

//
//  ScheduleViewModel.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/5/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import Foundation
import Combine
import NHLAPI

class ScheduleViewModel : ObservableObject {
    @Published var schedule = Schedule()

    var nhlScheduleRangeActivitySubscriber: AnyCancellable?
    var nhlScheduleRangeDataSubscriber: AnyCancellable?

    init() {
        self.reloadSchedule()
    }
    
    func reloadSchedule() {
        let apiActivitySub = NHLAPI.nhlScheduleRangeActivityPublisher
        .receive(on: RunLoop.main)
            .sink { doingSomethingNow in
                if (doingSomethingNow) { print("start scheduleRange activity...") }
                else { print("stop scheduleRange activity...") }
        }
        if nhlScheduleRangeActivitySubscriber == nil {
            nhlScheduleRangeActivitySubscriber = AnyCancellable(apiActivitySub)
        }
        
        nhlScheduleRangeDataSubscriber = NHLAPI.nhlScheduleRangeDataPublisher(startDate: Date.yesterday, endDate: Date.tomorrow)
            //NHLAPI.nhlScheduleRangeDataPublisher
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
            switch completion {
            case .failure(let error):
                print(error)
            case .finished:
                print("DONE - scheduleRange postDataPublisher")
            }
        }, receiveValue: { (schedule) in
            if let sched = schedule {
                self.schedule = sched
            }
        })
    }
}

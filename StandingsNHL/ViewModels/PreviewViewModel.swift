//
//  PreviewViewModel.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/14/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import UIKit
import Combine
import NHLAPI

class PreviewViewModel : ObservableObject {
    var preview : Articles
    @Published var previewHeadline : String = "(No headline available)"
    @Published var previewSubhead : String = ""
        
    init(preview : Articles) {
        self.preview = preview
       
        if self.preview.items != nil && self.preview.items!.count > 0 {
            self.previewHeadline = self.preview.items![0].headline ?? "(No headline available)"
            self.previewSubhead = self.preview.items![0].subhead ?? ""
        }
    }
}

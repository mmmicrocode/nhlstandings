//
//  WildCardRecordViewModel.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 1/27/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import Foundation
import NHLAPI

class WildCardConferenceRecordViewModel : ObservableObject, Identifiable {
    var standingsType: String?
    //var records : [Record]
    var league : Simple
    var conference : Simple?
    var wildCardTeamRecords : [TeamRecordViewModel]
    var leaderDivisionRecords : [DivisionRecordViewModel]

    var id: Int {
        return self.conference?.id ?? 0
    }
    var conferenceName: String {
        return self.conference?.name ?? "n/a"
    }
    
    // A bit different - need to pass in multiple Record objects and filter the wildCard / divisionLeaders
    init(records: [Record]) {
        self.league = records[0].league
        self.conference = records[0].conference
        
        // Get the wildCard Record
        let wildCard = records.filter( {
            $0.standingsType == .wildCard
        })
        self.wildCardTeamRecords = wildCard[0].teamRecords.map { TeamRecordViewModel.init(record: $0) }

        // Get the divisionLeaders Records
        self.leaderDivisionRecords = records.filter( { $0.standingsType == .divisionLeaders }).map( { DivisionRecordViewModel(record: $0) } )
    }
}

//class DivisionLeadersConferenceRecordViewModel : ObservableObject, Identifiable {
//    var standingsType: String?
//    var record : Record
//    var league : Simple
//    var conference : Simple?
//    var teamRecords = [TeamRecordViewModel]()
//
//    var id: Int {
//        return self.conference?.id ?? 0
//    }
//    var conferenceName: String {
//        return self.conference?.name ?? "n/a"
//    }
//    
//    init(record: Record) {
//        self.record = record
//        self.league = self.record.league
//        self.conference = self.record.conference
//        self.teamRecords = self.record.teamRecords.map { TeamRecordViewModel.init(record: $0) }
//    }
//}

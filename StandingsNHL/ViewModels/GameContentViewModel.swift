//
//  GameContentViewModel.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/11/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import Foundation
import Combine
import NHLAPI

class GameContentViewModel : ObservableObject {
    @Published var gameContent = GameContent()
    @Published var gameTitle : String?
    @Published var recapVM : RecapViewModel?
    @Published var highlightVMs : [HighlightViewModel]?
    @Published var previewVM : PreviewViewModel?
    
    var nhlGameContentActivitySubscriber: AnyCancellable?
    var nhlGameContentDataSubscriber: AnyCancellable?

    init() {
        //self.reloadGameContent()
    }
    
    func reloadGameContent(gameId: Int) {
        let apiActivitySub = NHLAPI.nhlGameContentActivityPublisher
        .receive(on: RunLoop.main)
            .sink { doingSomethingNow in
                if (doingSomethingNow) { print("start gameContent activity...") }
                else { print("stop gameContent activity...") }
        }
        if nhlGameContentActivitySubscriber == nil {
            nhlGameContentActivitySubscriber = AnyCancellable(apiActivitySub)
        }
        
        nhlGameContentDataSubscriber = NHLAPI.nhlGameContentDataPublisher(gameId: gameId)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
            switch completion {
            case .failure(let error):
                print(error)
            case .finished:
                print("DONE - gameContent postDataPublisher")
            }
        }, receiveValue: { (gameContent) in
            if let content = gameContent {
                self.gameContent = content

                // Preview content
                if let preview = self.gameContent.editorial?.preview {
                    self.previewVM = PreviewViewModel(preview: preview)
                }

                // Recap Video
                if let epgArticles = self.gameContent.media?.epg {
                    let recapArticles = epgArticles.filter { $0.title == "Recap" }
                    if recapArticles.count > 0, let items = recapArticles[0].items, items.count > 0 {
                        self.recapVM = RecapViewModel(recapItem: items[0])
                    }
                }

                // Highlight Video
                if let gameCenterArticles = self.gameContent.highlights?.gameCenter {
                    if let items = gameCenterArticles.items, items.count > 0 {
                        self.highlightVMs = items.map { HighlightViewModel(highlightItem: $0) }
                    }
                }
            }
        })
    }
}

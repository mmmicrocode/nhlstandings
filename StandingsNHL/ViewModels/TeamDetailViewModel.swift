//
//  TeamDetailViewModel.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 1/28/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import Foundation
import Combine
import NHLAPI

class TeamDetailViewModel : ObservableObject, Identifiable {
    @Published var teamDetail : TeamDetail? = nil
    @Published var teamRoster : TeamRoster? = nil

    var teamRecord : TeamRecordViewModel? = nil

    var teamDetailActivitySubscriber: AnyCancellable?
    var teamDetailDataSubscriber: AnyCancellable?
    var teamRosterActivitySubscriber: AnyCancellable?
    var teamRosterDataSubscriber: AnyCancellable?
    
    func reloadTeamDetails(id: Int) {
        let apiActivitySub = NHLAPI.nhlTeamDetailActivityPublisher
        .receive(on: RunLoop.main)
            .sink { doingSomethingNow in
                if (doingSomethingNow) {
                    print("start teamDetail activity...")
                } else {
                    print("stop teamDetail activity...")
                }
        }
        if teamDetailActivitySubscriber == nil {
            teamDetailActivitySubscriber = AnyCancellable(apiActivitySub)
        }
        
        teamDetailDataSubscriber = NHLAPI.nhlTeamDetailDataPublisher(teamId: id)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
            switch completion {
            case .failure(let error):
                print(error)
            case .finished:
                print("DONE - teamDetail postDataPublisher")
                //self.divisionRecords = self.localDivisionRecords
            }
        }, receiveValue: { (details) in
            if details.count > 0 {
                self.teamDetail = details[0]
            }
        })
    }

    func reloadTeamRoster(id: Int) {
        let apiActivitySub = NHLAPI.nhlTeamRosterActivityPublisher
        .receive(on: RunLoop.main)
            .sink { doingSomethingNow in
                if (doingSomethingNow) {
                    print("start teamRoster activity...")
                } else {
                    print("stop teamRoster activity...")
                }
        }
        if teamRosterActivitySubscriber == nil {
            teamRosterActivitySubscriber = AnyCancellable(apiActivitySub)
        }
        
        teamRosterDataSubscriber = NHLAPI.nhlTeamRosterDataPublisher(teamId: id)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
            switch completion {
            case .failure(let error):
                print(error)
            case .finished:
                print("DONE - teamRoster postDataPublisher")
            }
        }, receiveValue: { (rosters) in
            if rosters.count > 0 {
                self.teamRoster = rosters[0]
            }
        })
    }

    init() {
        //reloadTeamDetails()
    }
}

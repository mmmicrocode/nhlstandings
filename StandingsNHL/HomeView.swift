//
//  HomeView.swift
//  StandingsNHL
//
//  Created by Heil, Steven on 2/5/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import SwiftUI

enum TabActive : Int {
    case Standings = 0
    case Schedule = 1
}

#if os(watchOS)
typealias HomeView = WatchHomeView
#else
typealias HomeView = GenericHomeView
#endif

#if !os(watchOS)
struct GenericHomeView: View {
    var body : some View {
        TabView {
            StandingsView()
                .tabItem {
                    Image(systemName: "list.number")
                        .font(.title)
                    //Text("All Events")
            }.tag(TabActive.Standings)

            ScheduleView()
                .tabItem {
                    Image(systemName: "calendar")
                        .font(.title)
                    //Text("My Active Event")
                }.tag(TabActive.Schedule)
        }
    }
}
#endif

enum ViewTypeSelection : Int {
    case Standings = 0
    case Schedule = 1
}

struct WatchHomeView: View {
    @State var viewType : ViewTypeSelection = .Standings

    var body : some View {
        //StandingsView()
        
        List {
            NavigationLink(destination: StandingsView()) {
                HStack(spacing: 16.0) {
                    Image(systemName: "list.number")
                        .font(.largeTitle)
                    Text("Standings")
                        .font(.headline)
                }
                .frame(height: 70.0)
            }
            NavigationLink(destination: ScheduleView()) {
                HStack(spacing: 16.0) {
                    Image(systemName: "calendar")
                        .font(.largeTitle)
                    Text("Schedule")
                        .font(.headline)
                }
                .frame(height: 70.0)
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

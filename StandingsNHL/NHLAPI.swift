//
//  NHLAPI.swift
//
//  Created by Steve Heil on 10/30/19.
//

import Foundation
import Combine
import UserNotifications

@available(iOS 13.0, OSX 10.15, tvOS 13.0, *)
struct NHLAPI {

    static let apiUrlStandingsByDivision = "https://statsapi.web.nhl.com/api/v1/standings/byDivision"
    static let apiUrlStandingsByConference = "https://statsapi.web.nhl.com/api/v1/standings/byConference"
    static let apiUrlStandingsByWildCard = "https://statsapi.web.nhl.com/api/v1/standings/wildCardWithLeaders"
    static let apiUrlSchedule = "https://statsapi.web.nhl.com/api/v1/schedule"
    static let apiUrlTeamDetails = "https://statsapi.web.nhl.com/api/v1/teams/"
    static let apiUrlTeamRoster = "https://statsapi.web.nhl.com/api/v1/teams/"

    var session : URLSession
    
    init(withSession: URLSession = .shared) {
        session = withSession
    }

    //
    //MARK: - Division Standings
    //
    static let nhlStandingsByDivisionActivityPublisher = PassthroughSubject<Bool, Never>()

    static let nhlStandingsByDivisionDataPublisher = URLSession.shared.dataTaskPublisher(for: URL(string: NHLAPI.apiUrlStandingsByDivision)!)
        .handleEvents(receiveSubscription: { _ in
            nhlStandingsByDivisionActivityPublisher.send(true)
        }, receiveCompletion: { _ in
            nhlStandingsByDivisionActivityPublisher.send(false)
        }, receiveCancel: {
            nhlStandingsByDivisionActivityPublisher.send(false)
        })
        .tryMap { data, response -> Data in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                throw APIError.UnknownError
            }
            return data
        }.decode(type: Standings.self, decoder: NHLAPI.nhlDecodeStrategyDecoder)

    //
    //MARK: - Conference Standings
    //
    static let nhlStandingsByConferenceActivityPublisher = PassthroughSubject<Bool, Never>()

    static let nhlStandingsByConferenceDataPublisher = URLSession.shared.dataTaskPublisher(for: URL(string: NHLAPI.apiUrlStandingsByConference)!)
        .handleEvents(receiveSubscription: { _ in
            nhlStandingsByConferenceActivityPublisher.send(true)
        }, receiveCompletion: { _ in
            nhlStandingsByConferenceActivityPublisher.send(false)
        }, receiveCancel: {
            nhlStandingsByConferenceActivityPublisher.send(false)
        })
        .tryMap { data, response -> Data in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                throw APIError.UnknownError
            }
            return data
        }.decode(type: Standings.self, decoder: NHLAPI.nhlDecodeStrategyDecoder)

    //
    //MARK: - WildCard Standings
    //
    static let nhlStandingsByWildCardActivityPublisher = PassthroughSubject<Bool, Never>()

    static let nhlStandingsByWildCardDataPublisher = URLSession.shared.dataTaskPublisher(for: URL(string: NHLAPI.apiUrlStandingsByWildCard)!)
        .handleEvents(receiveSubscription: { _ in
            nhlStandingsByWildCardActivityPublisher.send(true)
        }, receiveCompletion: { _ in
            nhlStandingsByWildCardActivityPublisher.send(false)
        }, receiveCancel: {
            nhlStandingsByWildCardActivityPublisher.send(false)
        })
        .tryMap { data, response -> Data in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                throw APIError.UnknownError
            }
            return data
        }.decode(type: Standings.self, decoder: NHLAPI.nhlDecodeStrategyDecoder)

    //
    //MARK: - Today Schedule
    //
    static let nhlTodayScheduleActivityPublisher = PassthroughSubject<Bool, Never>()

    static let nhlTodayScheduleDataPublisher = URLSession.shared.dataTaskPublisher(for: URL(string: NHLAPI.apiUrlSchedule)!)
        .handleEvents(receiveSubscription: { _ in
            nhlTodayScheduleActivityPublisher.send(true)
        }, receiveCompletion: { _ in
            nhlTodayScheduleActivityPublisher.send(false)
        }, receiveCancel: {
            nhlTodayScheduleActivityPublisher.send(false)
        })
        .tryMap { data, response -> Data in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                throw APIError.UnknownError
            }
            return data
        }.decode(type: Schedule.self, decoder: NHLAPI.nhlDecodeStrategyDecoder)

    //
    //MARK: - Team Details
    //
    static let nhlTeamDetailActivityPublisher = PassthroughSubject<Bool, Never>()

    static func nhlTeamDetailPublisher(teamId: Int) -> AnyPublisher<[TeamDetail], Never> {
        if teamId < 1 {
            return Just([]).eraseToAnyPublisher()
            //return Publishers.Empty<TeamDetail, Never>().eraseToAnyPublisher()
        }
        let assembledURL = String("\(NHLAPI.apiUrlTeamDetails)\(teamId)")
        
        let publisher = URLSession.shared.dataTaskPublisher(for: URL(string: assembledURL)!)
        .handleEvents(receiveSubscription: { _ in
            nhlTeamDetailActivityPublisher.send(true)
        }, receiveCompletion: { _ in
            nhlTeamDetailActivityPublisher.send(false)
        }, receiveCancel: {
            nhlTeamDetailActivityPublisher.send(false)
        })
        .tryMap { data, response -> Data in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                throw APIError.UnknownError
            }
            return data
        }
        .decode(type: TeamDetail.self, decoder: NHLAPI.nhlDecodeStrategyDecoder)
        .map { [$0] }
            .catch { err in
                //return Publishers.Empty<TeamDetail, Never>()
                return Just([])
            }
        .eraseToAnyPublisher()
        return publisher
    }

    //
    //MARK: - Team Roster
    //
    static let nhlTeamRosterActivityPublisher = PassthroughSubject<Bool, Never>()

    static func nhlTeamRosterPublisher(teamId: Int) -> AnyPublisher<[TeamRoster], Never> {
        if teamId < 1 {
            return Just([]).eraseToAnyPublisher()
            //return Publishers.Empty<TeamDetail, Never>().eraseToAnyPublisher()
        }
        let assembledURL = String("\(NHLAPI.apiUrlTeamRoster)\(teamId)/roster")
        
        let publisher = URLSession.shared.dataTaskPublisher(for: URL(string: assembledURL)!)
        .handleEvents(receiveSubscription: { _ in
            nhlTeamRosterActivityPublisher.send(true)
        }, receiveCompletion: { _ in
            nhlTeamRosterActivityPublisher.send(false)
        }, receiveCancel: {
            nhlTeamRosterActivityPublisher.send(false)
        })
        .tryMap { data, response -> Data in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                throw APIError.UnknownError
            }
            return data
        }
        .decode(type: TeamRoster.self, decoder: NHLAPI.nhlDecodeStrategyDecoder)
        .map { [$0] }
            .catch { err in
                //return Publishers.Empty<TeamDetail, Never>()
                return Just([])
            }
        .eraseToAnyPublisher()
        return publisher
    }

    //MARK: - Custom JSON Decoder
    
    static var nhlDecodeStrategyDecoder : JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .nhlapiDateDecodingStrategy
        return decoder
    }
}

//MARK: - Custom Types

enum APIError : Error {
    case ConnectionError(error: Error)
    case NoDataError
    case JSONDecodeError(error: Error)
    case UnknownError
}

//MARK: - Relevant Object Extensions

extension Notification.Name {
    static var NHLDivisionStandingsUpdated = Notification.Name("NHLDivisionStandingsUpdated")
    static var NHLTodayScheduleUpdated = Notification.Name("NHLTodayScheduleUpdated")
}

// We are extending the DateFormatter and DateDecodingStrategy to handle and properly decode
// the various Date formats in the NHL API data.
extension Formatter {
    static let nhlapiGameDate: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.calendar = Calendar(identifier: .iso8601)
        //formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
    static let nhlapiShortDate: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = Calendar(identifier: .iso8601)
        //formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
}

extension JSONDecoder.DateDecodingStrategy {
    static let nhlapiDateDecodingStrategy = custom {
        let container = try $0.singleValueContainer()
        let string = try container.decode(String.self)
        if let date = Formatter.nhlapiShortDate.date(from: string)
                    ?? Formatter.nhlapiGameDate.date(from: string) {
            return date
        }
        throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid date: \(string)")
    }
}

//    static let nhlStandingsCancellableSink = NHLAPI.nhlStandingsDataPublisher
//        .sink(receiveCompletion: { completion in
//                print(".sink() received the completion", String(describing: completion))
//                switch completion {
//                    case .finished:
//                        break
//                    case .failure(let anError):
//                        print("received error: ", anError)
//                }
//        }, receiveValue: { someValue in
//            print(".sink() received \(someValue)")
//        })

// MARK: - Schedule
struct Schedule : Codable {
    let copyright: String?
    let totalItems, totalEvents, totalGames, totalMatches: Int?
    let wait: Int?
    let dates: [DateElement]?
}

// MARK: - DateElement
struct DateElement : Codable {
    let date: Date?
    let totalItems, totalEvents, totalGames, totalMatches: Int?
    let games: [Game]?
    //let events, matches: []?
    
    private enum CodingKeys: String, CodingKey {
         case date, totalItems, totalEvents, totalGames, totalMatches, games
     }

}

// MARK: - Game
struct Game : Codable {
    let gamePk: Int?
    let link, gameType, season: String?
    let gameDate: Date?
    let status: Status?
    let teams: Teams?
    let venue: Venue?
    let content: Content?
}

// MARK: - Content
struct Content : Codable {
    let link: String?
}

// MARK: - Status
struct Status : Codable {
    let abstractGameState, codedGameState, detailedState, statusCode: String?
    let startTimeTBD: Bool?
}

// MARK: - Teams
struct Teams : Codable {
    let away, home: TeamAndScore?
}

// MARK: - Away
struct TeamAndScore : Codable {
    let leagueRecord: LeagueRecord?
    let score: Int?
    let team: Venue?
}


// MARK: - Venue
struct Venue : Codable {
    let id: Int?
    let name, link, city: String?
    let timeZone: TimeZone?
}

// MARK: - Standings
struct Standings : Codable {
    let copyright: String
    let records: [Record]
    var timestamp: Date?
    
    private enum CodingKeys: String, CodingKey {
        case copyright, records
    }
}

// MARK: - Record
enum StandingsType : String, Codable {
    case regularSeason = "regularSeason"
    case wildCard = "wildCard"
    case byConference = "byConference"
    case byDivision = "byDivision"
    case divisionLeaders = "divisionLeaders"
}

struct Record : Codable {
    let standingsType: StandingsType
    let league: Simple
    let division: Expanded?
    let conference: Simple?
    let teamRecords: [TeamRecord]
}

// MARK: - Simple
struct Simple : Codable {
    let id: Int
    let name, link: String
}

// MARK: - Expanded
struct Expanded : Codable {
    let id: Int
    let name, nameShort, link, abbreviation: String
}

// MARK: - TeamRecord

// MARK: - Team
struct Team : Codable {
    let id: Int?
    let name, link: String?
    let venue: Venue?
    let teamName, locationName, firstYearOfPlay: String?
    let division: Division?
    let conference: Conference?
    let franchise: Franchise?
    let shortName: String?
    let officialSiteURL: String?
    let franchiseID: Int?
    let active: Bool?

//struct Team : Codable {
//    let id: Int
//    let name, link: String

    private enum CodingKeys: String, CodingKey {
        case id, name, link, venue, teamName, locationName, firstYearOfPlay,
            division, conference, franchise, shortName, officialSiteURL, franchiseID, active
    }

    var city : String {
        switch(self.id) {
        case 15: return "Washington"
        case 12: return "Carolina"
        case 5: return "Pittsburgh"
        case 2: return "New York"
        case 29: return "Columbus"
        case 4: return "Philadelphia"
        case 1: return "New Jersey"
        case 3: return "New York"
        case 7: return "Buffalo"
        case 6: return "Boston"
        case 10: return "Toronto"
        case 14: return "Tampa Bay"
        case 13: return "Florida"
        case 8: return "Montréal"
        case 17: return "Detroit"
        case 9: return "Ottawa"
        case 21: return "Colorado"
        case 18: return "Nashville"
        case 19: return "St. Louis"
        case 52: return "Winnipeg"
        case 25: return "Dallas"
        case 16: return "Chicago"
        case 30: return "Minnesota"
        case 22: return "Edmonton"
        case 54: return "Vegas"
        case 23: return "Vancouver"
        case 24: return "Anaheim"
        case 53: return "Arizona"
        case 20: return "Calgary"
        case 26: return "Los Angeles"
        case 28: return "San Jose"
        default: return ""
        }
    }
    var nickname : String {
        switch(self.id) {
        case 15: return "Capitols"
        case 12: return "Hurricanes"
        case 5: return "Penguins"
        case 2: return "Islanders"
        case 29: return "Blue Jackets"
        case 4: return "Flyers"
        case 1: return "Devils"
        case 3: return "Rangers"
        case 7: return "Sabres"
        case 6: return "Bruins"
        case 10: return "Maple Leafs"
        case 14: return "Lightning"
        case 13: return "Panthers"
        case 8: return "Canadiens"
        case 17: return "Red Wings"
        case 9: return "Senators"
        case 21: return "Avalanche"
        case 18: return "Predators"
        case 19: return "Blues"
        case 52: return "Jets"
        case 25: return "Stars"
        case 16: return "Blackhawks"
        case 30: return "Wild"
        case 22: return "Oilers"
        case 54: return "Golden Knights"
        case 23: return "Canucks"
        case 24: return "Ducks"
        case 53: return "Coyotes"
        case 20: return "Flames"
        case 26: return "Kings"
        case 28: return "Sharks"
        default: return ""
        }
    }
    var abbreviation : String {
        switch(self.id) {
        case 15: return "WSH"
        case 12: return "CAR"
        case 5: return "PIT"
        case 2: return "NYI"
        case 29: return "CBJ"
        case 4: return "PHI"
        case 1: return "NJD"
        case 3: return "NYI"
        case 7: return "BUF"
        case 6: return "BOS"
        case 10: return "TOR"
        case 14: return "TBL"
        case 13: return "FLA"
        case 8: return "MTL"
        case 17: return "DET"
        case 9: return "OTT"
        case 21: return "COL"
        case 18: return "NSH"
        case 19: return "STL"
        case 52: return "WPG"
        case 25: return "DAL"
        case 16: return "CHI"
        case 30: return "MIN"
        case 22: return "EDM"
        case 54: return "VGK"
        case 23: return "VAN"
        case 24: return "ANH"
        case 53: return "ARI"
        case 20: return "CGY"
        case 26: return "LAK"
        case 28: return "SJS"
        default: return ""
        }
    }
//    var image : UIImage? {
//        return UIImage(named: self.name)
//    }
//    var cgimage : CGImage? {
//        return self.image?.cgImage
//    }
}

struct TeamRecord : Codable {
    let team: Team
    let leagueRecord: LeagueRecord
    let goalsAgainst, goalsScored, points: Int
    let divisionRank, divisionL10Rank, divisionRoadRank, divisionHomeRank: String
    let conferenceRank, conferenceL10Rank, conferenceRoadRank, conferenceHomeRank: String
    let leagueRank, leagueL10Rank, leagueRoadRank, leagueHomeRank: String
    let wildCardRank: String
    let row, gamesPlayed: Int
    let streak: Streak
    let lastUpdated: Date
}

// MARK: - LeagueRecord
struct LeagueRecord : Codable {
    let wins, losses, ot: Int
    let type: LeagueRecordType
}

enum LeagueRecordType : String, Codable {
    case league = "league"
}

// MARK: - Streak
struct Streak : Codable {
    let streakType: StreakType
    let streakNumber: Int
    let streakCode: String
}

enum StreakType : String, Codable {
    case losses = "losses"
    case ot = "ot"
    case wins = "wins"
}

// MARK: - TeamDetail
struct TeamDetail : Codable {
    let copyright: String?
    let teams: [Team]?
}

// MARK: - Conference
struct Conference : Codable {
    let id: Int?
    let name, link: String?
}

// MARK: - Division
struct Division : Codable {
    let id: Int?
    let name, nameShort, link, abbreviation: String?
}

// MARK: - Franchise
struct Franchise : Codable {
    let franchiseID: Int?
    let teamName, link: String?
}

// MARK: - TimeZone
struct TimeZone : Codable {
    let id: String?
    let offset: Int?
    let tz: String?
}

// TEAM ROSTER

// MARK: - Roster
struct TeamRoster: Codable {
    let copyright: String
    let roster: [RosterElement]
    let link: String
}

// MARK: - RosterElement
struct RosterElement: Codable {
    let person: Person
    let jerseyNumber: String
    let position: Position
}

// MARK: - Person
struct Person: Codable {
    let id: Int
    let fullName, link: String
}

// MARK: - Position
struct Position: Codable {
    let code, name: String
    let type: TypeEnum
    let abbreviation: String
}

enum TypeEnum: String, Codable {
    case defenseman = "Defenseman"
    case forward = "Forward"
    case goalie = "Goalie"
}


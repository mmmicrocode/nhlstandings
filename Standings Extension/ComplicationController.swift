//
//  ComplicationController.swift
//  Standings Extension
//
//  Created by Heil, Steven on 1/3/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import ClockKit


class ComplicationController: NSObject, CLKComplicationDataSource {
    
    // MARK: - Timeline Configuration
    
    func getSupportedTimeTravelDirections(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Void) {
        handler([.forward, .backward])
    }
    
    func getTimelineStartDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        handler(nil)
    }
    
    func getTimelineEndDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        handler(nil)
    }
    
    func getPrivacyBehavior(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.showOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Void) {
        // Call the handler with the current timeline entry
//        let template = CLKComplicationTemplate()
//        let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)

        let teamName = CLKSimpleTextProvider.localizableTextProvider(withStringsFileTextKey: "Penguins")
        let teamPos = CLKSimpleTextProvider.localizableTextProvider(withStringsFileTextKey: "2nd")

        switch complication.family {
            case .modularSmall:
                let template = CLKComplicationTemplateModularSmallStackText()
                template.line1TextProvider = teamName
                template.line2TextProvider = teamPos
                let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
                handler(entry)
            case .modularLarge:
                let template = CLKComplicationTemplateModularLargeTallBody()
                template.bodyTextProvider = teamPos
                template.headerTextProvider = teamName
                let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
                handler(entry)
            case .circularSmall:
                let template = CLKComplicationTemplateCircularSmallSimpleText()
                template.textProvider = teamPos
                let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
                handler(entry)
            case .extraLarge:
                let template = CLKComplicationTemplateExtraLargeStackText()
                template.line1TextProvider = teamName
                template.line2TextProvider = teamPos
                let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
                handler(entry)
            case .utilitarianSmall, .utilitarianSmallFlat:
                let template = CLKComplicationTemplateUtilitarianSmallFlat()
                template.textProvider = teamPos
                let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
                handler(entry)
            case .utilitarianLarge:
                let template = CLKComplicationTemplateUtilitarianLargeFlat()
                //let eventualText = CLKSimpleTextProvider.localizableTextProvider(withStringsFileTextKey: "75")
                template.textProvider = CLKSimpleTextProvider.localizableTextProvider(withStringsFileFormatKey: "UtilitarianLargeFlat", textProviders: [teamName, teamPos, CLKTimeTextProvider(date: Date())])
                let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
                handler(entry)
            case .graphicCorner:
                if #available(watchOSApplicationExtension 5.0, *) {
                    let template = CLKComplicationTemplateGraphicCornerStackText()
                    teamPos.tintColor = .lightGray
                    template.innerTextProvider = teamName
                    template.outerTextProvider = teamPos
                    let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
                    handler(entry)
                } else {
                    handler(nil)
                }
            case .graphicCircular:
                if #available(watchOSApplicationExtension 5.0, *) {
                    let template = CLKComplicationTemplateGraphicCircularOpenGaugeSimpleText()
                    template.centerTextProvider = teamName
                    template.bottomTextProvider = CLKSimpleTextProvider(text: "↘︎")
                    template.gaugeProvider = CLKSimpleGaugeProvider(style: .fill, gaugeColor: .lightGray, fillFraction: 1)
                    let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
                    handler(entry)
                } else {
                    handler(nil)
                }
            case .graphicBezel:
                handler(nil)
            case .graphicRectangular:
                if #available(watchOSApplicationExtension 5.0, *) {
                    let template = CLKComplicationTemplateGraphicRectangularLargeImage()
                    // TODO: Better placeholder image here
                    template.imageProvider = CLKFullColorImageProvider(fullColorImage: UIImage())
                    template.textProvider = teamPos
                    let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
                    handler(entry)
                } else {
                    handler(nil)
                }
            @unknown default:
                handler(nil)
            }
    }
    
    func getTimelineEntries(for complication: CLKComplication, before date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries prior to the given date
        handler(nil)
    }
    
    func getTimelineEntries(for complication: CLKComplication, after date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries after to the given date
        handler(nil)
    }
    
    // MARK: - Placeholder Templates

    func getLocalizableSampleTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
        let template = getLocalizableSampleTemplate(for: complication.family)
        handler(template)
    }

    func getLocalizableSampleTemplate(for family: CLKComplicationFamily) -> CLKComplicationTemplate? {
        // This method will be called once per supported complication, and the results will be cached
        let teamName = CLKSimpleTextProvider.localizableTextProvider(withStringsFileTextKey: "Team 1")
        let teamPos = CLKSimpleTextProvider.localizableTextProvider(withStringsFileTextKey: "4th")
        
        switch family {
        case .modularSmall:
            let template = CLKComplicationTemplateModularSmallStackText()
            template.line1TextProvider = teamName
            template.line2TextProvider = teamPos
            return template
        case .modularLarge:
            let template = CLKComplicationTemplateModularLargeTallBody()
            template.bodyTextProvider = teamPos
            template.headerTextProvider = teamName
            return template
        case .circularSmall:
            let template = CLKComplicationTemplateCircularSmallSimpleText()
            template.textProvider = teamPos
            return template
        case .extraLarge:
            let template = CLKComplicationTemplateExtraLargeStackText()
            template.line1TextProvider = teamName
            template.line2TextProvider = teamPos
            return template
        case .utilitarianSmall, .utilitarianSmallFlat:
            let template = CLKComplicationTemplateUtilitarianSmallFlat()
            template.textProvider = teamPos
            return template
        case .utilitarianLarge:
            let template = CLKComplicationTemplateUtilitarianLargeFlat()
            //let eventualText = CLKSimpleTextProvider.localizableTextProvider(withStringsFileTextKey: "75")
            template.textProvider = CLKSimpleTextProvider.localizableTextProvider(withStringsFileFormatKey: "UtilitarianLargeFlat", textProviders: [teamName, teamPos, CLKTimeTextProvider(date: Date())])
            return template
        case .graphicCorner:
            if #available(watchOSApplicationExtension 5.0, *) {
                let template = CLKComplicationTemplateGraphicCornerStackText()
                teamPos.tintColor = .lightGray
                template.innerTextProvider = teamName
                template.outerTextProvider = teamPos
                return template
            } else {
                return nil
            }
        case .graphicCircular:
            if #available(watchOSApplicationExtension 5.0, *) {
                let template = CLKComplicationTemplateGraphicCircularOpenGaugeSimpleText()
                template.centerTextProvider = teamName
                template.bottomTextProvider = CLKSimpleTextProvider(text: "↘︎")
                template.gaugeProvider = CLKSimpleGaugeProvider(style: .fill, gaugeColor: .lightGray, fillFraction: 1)
                return template
            } else {
                return nil
            }
        case .graphicBezel:
            return nil
//            if #available(watchOSApplicationExtension 5.0, *) {
//                let template = CLKComplicationTemplateGraphicBezelCircularText()
//                guard let circularTemplate = getLocalizableSampleTemplate(for: .graphicCircular, withHandler: handler) as? CLKComplicationTemplateGraphicCircular else {
//                    fatalError("\(#function) invoked with .graphicCircular must return a subclass of CLKComplicationTemplateGraphicCircular")
//                }
//                template.circularTemplate = circularTemplate
//                template.textProvider = teamPos
//                return handler(template)
//            } else {
//                return handler(nil)
//            }
        case .graphicRectangular:
            if #available(watchOSApplicationExtension 5.0, *) {
                let template = CLKComplicationTemplateGraphicRectangularLargeImage()
                // TODO: Better placeholder image here
                template.imageProvider = CLKFullColorImageProvider(fullColorImage: UIImage())
                template.textProvider = teamPos
                return template
            } else {
                return nil
            }
        @unknown default:
            return nil
        }
    }
}

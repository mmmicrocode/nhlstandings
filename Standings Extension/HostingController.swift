//
//  HostingController.swift
//  Standings Extension
//
//  Created by Heil, Steven on 1/3/20.
//  Copyright © 2020 Thermo Fisher Scientific. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<HomeView> {
    override var body: HomeView {
        return HomeView()
    }
}
